import java.util.Scanner;

public class MemoryNuevo {

	static Scanner scn = new Scanner(System.in);

	static int MIDA = 4;

	public static void main(String[] args) {

		joc();

	}

	private static void joc() {

		int puntos1 = 0;
		int puntos2 = 0; 
		boolean jugador = true;
		char[][] secret = new char[MIDA][MIDA];
		char[][] tauler = new char[MIDA][MIDA];
		// inicializacion de los tableros
		tauler = inicialitzaTauler(tauler, MIDA);
		mostraTauler(tauler, MIDA);
		secret = inicialitzaSecret(secret, MIDA);
		do {
			System.out.println("LOS PUNTOS DEL JUGADOR 1: " + puntos1);
			System.out.println("LOS PUNTOS DEL JUGADOR 2: " + puntos2);
			if (jugador == true) {
				if (torn(tauler, secret, MIDA) == true) {
					puntos1++;
				} else
					jugador=false;
			}
			if (jugador == false) {
				if (torn(tauler, secret, MIDA) == true) {
					puntos2++;
				} else
					jugador=true;
			}

		} while ((puntos1 + puntos2) < (MIDA * MIDA / 2));
		
		


	}

	private static char[][] inicialitzaTauler(char[][] tauler, int MIDA) {

		// Recorre el tablero i lo rellena con "?"

		for (int i = 0; i < MIDA; i++)
			for (int j = 0; j < MIDA; j++)
				tauler[i][j] = '?';

		return tauler;

	}

	private static void mostraTauler(char[][] tauler, int MIDA) {

		// Introduce la numeracion superior del tablero
//		System.out.print("  ");
//		for (int i = 1; i <= MIDA; i++)
//			System.out.print(i);

		System.out.println("   ");

		// Recorre el tablero i lo muestra de uno en uno
		for (int i = 0; i < MIDA; i++) {
			System.out.print((i + 1) + " ");
			for (int j = 0; j < MIDA; j++)
				System.out.print(tauler[i][j]);

			System.out.println("");

		}

	}

	private static char[][] posaPeces(char[][] secret, int MIDA) {

		int count = 0;
		int numLetra = 0;

		// Rellena el tablero de letras

		for (int i = 0; i < MIDA; i++)
			for (int j = 0; j < MIDA; j++) {
				secret[i][j] = (char) ('A' + numLetra);
				count++;
				if (count == 2) {
					numLetra++;
					count = 0;
				}
			}

		// Coloca los numeros de la parte superior

		System.out.print("  ");
		for (int i = 1; i <= MIDA; i++)
			System.out.print(i);

		System.out.println("   ");

		// Recorre el tablero i lo muestra de uno en uno

//		for (int i = 0; i < MIDA; i++) {
//			System.out.print((i + 1) + " ");
//			for (int j = 0; j < MIDA; j++)
//				System.out.print(secret[i][j]);
//
//			System.out.println("");

//		}
		return secret;

	}

	private static char[][] remenaPeces(char[][] secret, int MIDA) {

		int filaA;
		int columnaA;
		char letraG = 'a';
		char[] letras = new char[26];

		// Mezcla las letras
		for (int i = 0; i < MIDA; i++)
			for (int j = 0; j < MIDA; j++) {
				filaA = (int) (Math.random() * 4);
				columnaA = (int) (Math.random() * 4);
				letraG = secret[filaA][columnaA];
				secret[filaA][columnaA] = secret[i][j];
				secret[i][j] = letraG;
			}

		System.out.println(" ");

		// Coloca los numeros de la parte superior

//		System.out.print("  ");
//		for (int i = 1; i <= MIDA; i++)
//			System.out.print(i);
//
//		System.out.println("   ");

		// Recorre el tablero i lo muestra de uno en uno

//		for (int i = 0; i < MIDA; i++) {
//			System.out.print((i + 1) + " ");
//			for (int j = 0; j < MIDA; j++)
//				System.out.print(secret[i][j]);
//
//			System.out.println("");
//
//		}
		return secret;
	}

	private static char[][] inicialitzaSecret(char[][] secret, int MIDA) {

		secret = posaPeces(secret, MIDA);
		secret = remenaPeces(secret, MIDA);

		return secret;

	}

	private static int validarDada(int MIDA, char[][] tauler) {

		int Coordenada = 0;

		do {
			System.out.println(" Introduce una posicion : ");
			Coordenada = scn.nextInt();
		} while (Coordenada > MIDA || Coordenada <= 0);

		return Coordenada;
	}

	private static boolean validarCasella(int fila, int columna, char[][] tauler) {

		if (tauler[fila][columna] == '?') {
			return true;
		} else
			return false;
	}

	private static boolean torn(char[][] tauler, char[][] secret, int MIDA) {

		int posicioF1;
		int posicioC1;
		int posicioF2;
		int posicioC2;

		// primera coordenada
		do {
			posicioF1 = validarDada(MIDA, tauler)-1;
			posicioC1 = validarDada(MIDA, tauler)-1;
		} while (validarCasella(posicioF1, posicioC1, tauler) == false);

		tauler[posicioF1][posicioC1] = secret[posicioF1][posicioC1];

		mostraTauler(tauler, MIDA);

		// seginda coordemada
		do {
			posicioF2 = validarDada(MIDA, tauler)-1;
			posicioC2 = validarDada(MIDA, tauler)-1;
		} while (validarCasella(posicioF2, posicioC2, tauler) == false);

		tauler[posicioF2][posicioC2] = secret[posicioF2][posicioC2];

		mostraTauler(tauler, MIDA);

		if (tauler[posicioF1][posicioC1] != tauler[posicioF2][posicioC2]) {
			tauler[posicioF1][posicioC1] = '?';
			tauler[posicioF2][posicioC2] = '?';
			return false;
		} else {
			return true;
		}

	}  

}
